#!/bin/bash

# The MIT License (MIT)
#
# Copyright (c) 2015 Microsoft Azure
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Trent Swanson (Full Scale 180 Inc)
#
help()
{
    echo "This script installs Elasticsearch cluster on Ubuntu"
    echo "Parameters:"
    echo "-n elasticsearch cluster name"
    echo "-d static discovery endpoints 10.0.0.1-3"
    echo "-v elasticsearch version"
    echo "-a storage account (for AFS)"
    echo "-k access key (for AFS)"
    echo "-c create and mount AFS share"
    echo "-m install marvel yes/no"
    echo "-e export marvel data to a different ip"
    echo "-w configure as a dedicated marvel node"
    echo "-x configure as a dedicated master node"
    echo "-y configure as client only node (no master, no data)"
    echo "-z configure as data node (no master)"
    echo "-s used striped data disk volumes"
    echo "-j install jmeter server agent"
    echo "-p install the cloud-azure plugin"
    echo "-o storage account (for cloud-azure)"
    echo "-r storage key (for cloud-azure)"
    echo "-h view this help content"
}

# Log method to control/redirect log output
log()
{
    # If you want to enable this logging add a un-comment the line below and add your account id
    #curl -X POST -H "content-type:text/plain" --data-binary "${HOSTNAME} - $1" https://logs-01.loggly.com/inputs/<key>/tag/es-extension,${HOSTNAME}
    echo "$1"
}

log "Begin execution of Elasticsearch script extension on ${HOSTNAME}"

if [ "${UID}" -ne 0 ];
then
    log "Script executed without root permissions"
    echo "You must be root to run this program." >&2
    exit 3
fi

# TEMP FIX - Re-evaluate and remove when possible
# This is an interim fix for hostname resolution in current VM
grep -q "${HOSTNAME}" /etc/hosts
if [ $? == 0 ]
then
  echo "${HOSTNAME}found in /etc/hosts"
else
  echo "${HOSTNAME} not found in /etc/hosts"
  # Append it to the hosts file if not there
  echo "127.0.0.1 ${HOSTNAME}" >> /etc/hosts
  log "hostname ${HOSTNAME} added to /etc/hosts"
fi

#Script Parameters
CLUSTER_NAME="elasticsearch"
ES_VERSION="5.2.2"
MARVEL_ONLY_NODE=0
DISCOVERY_ENDPOINTS=""
INSTALL_MARVEL=0
CLIENT_ONLY_NODE=0
DATA_NODE=0
MASTER_ONLY_NODE=0
USE_AFS=0
STORAGE_ACCOUNT=""
ACCESS_KEY=""
INSTALL_CLOUD_AZURE=0
CLOUD_AZURE_ACCOUNT=""
CLOUD_AZURE_KEY=""

JAVA_MIN="Xms1g"
JAVA_MAX="Xmx"

TOTAL_VM_MEM="$(awk '/MemTotal/ {print $2}' /proc/meminfo)"
TOTAL_VM_MEM_GB="$((($TOTAL_VM_MEM/1048576)/2))"
JAVA_MAX+=$TOTAL_VM_MEM_GB
JAVA_MAX+="g"

#Loop through options passed
while getopts :n:d:v:a:k:cme:o:r:pwxyzsjh optname; do
  log "Option $optname set with value ${OPTARG}"
  case $optname in
    n) #set cluster name
      CLUSTER_NAME=${OPTARG}
      ;;
    d) #static discovery endpoints
      DISCOVERY_ENDPOINTS=${OPTARG}
      ;;
    v) #elasticsearch version number
      ES_VERSION=${OPTARG}
      ;;
    m) #install marvel
      INSTALL_MARVEL=1
      ;;
    e) #export marvel data
      MARVEL_ENDPOINTS=${OPTARG}
      ;;
    w) #marvel node
      MARVEL_ONLY_NODE=1
      ;;
    x) #master node
      MASTER_ONLY_NODE=1
      ;;
    y) #client node
      CLIENT_ONLY_NODE=1
      ;;
    z) #data node
      DATA_NODE=1
      ;;
    s) #use OS striped disk volumes
      OS_STRIPED_DISK=1
      ;;
    a) #set the storage account for AFS
      STORAGE_ACCOUNT=${OPTARG}
      ;;
    k) #set the access key for AFS
      ACCESS_KEY=${OPTARG}
      ;;
    c) #use AFS for the data storage
      USE_AFS=1
      ;;
    d) #place data on local resource disk
      NON_DURABLE=1
      ;;
    j) #install jmeter server agent
      JMETER_AGENT=1
      ;;
    p) #install cloud-azure plugin
      INSTALL_CLOUD_AZURE=1
      ;;
    o) #set cloud-azure account
      CLOUD_AZURE_ACCOUNT=${OPTARG}
      ;;
    r) #set the cloud-azure account key
      CLOUD_AZURE_KEY=${OPTARG}
      ;;
    h) #show help
      help
      exit 2
      ;;
    \?) #unrecognized option - show help
      echo -e \\n"Option -${BOLD}$OPTARG${NORM} not allowed."
      help
      exit 2
      ;;
  esac
done

# Base path for data disk mount points
# The script assume format /datadisks/disk1 /datadisks/disk2
DATA_BASE="/datadisks"

# Expand a list of successive ip range and filter my local local ip from the list
# Ip list is represented as a prefix and that is appended with a zero to N index
# 10.0.0.1-3 would be converted to "10.0.0.10 10.0.0.11 10.0.0.12"

# Install Oracle Java
install_java()
{
    log "Installing Java"
    add-apt-repository -y ppa:webupd8team/java
    apt-get -y update  > /dev/null
    echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
    echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
    apt-get -y install oracle-java8-installer  > /dev/null
}

# Install Elasticsearch
install_es()
{
	
#	# Elasticsearch 2.x uses a different download path
#    if [[ "${ES_VERSION}" == \2* ]]; then
#        DOWNLOAD_URL="https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/$ES_VERSION/elasticsearch-$ES_VERSION.deb"
#    else
#        DOWNLOAD_URL="https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-$ES_VERSION.deb"
#    fi

    log "Installing Elaticsearch Version - $ES_VERSION"
	log "Download location - $DOWNLOAD_URL"
#    sudo wget -q "$DOWNLOAD_URL" -O elasticsearch.deb
    sudo wget -q "https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.2.deb" -O elasticsearch.deb
    sudo dpkg -i elasticsearch.deb
}

# Primary Install Tasks
#########################
#NOTE: These first three could be changed to run in parallel
#      Future enhancement - (export the functions and use background/wait to run in parallel)

install_cerebro()
{
    log "Installing Cerebro"
CEREBRO_VERSION="cerebro-0.6.3"
wget https://github.com/lmenezes/cerebro/releases/download/v0.6.3/$CEREBRO_VERSION.tgz
tar -zxvf $CEREBRO_VERSION.tgz
mv $CEREBRO_VERSION /opt/$CEREBRO_VERSION

log "Creating Cerebro Conf"
mv /opt/$CEREBRO_VERSION/conf/application.conf /opt/$CEREBRO_VERSION/conf/application.conf.orig


echo 'secret = "ki:s:[[@=Ag?QI`W2jMwkY:eqvrJ]JqoJyi2axj3ZvOv^/KavOT4ViJSv?6YY4[N"' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'basePath = "/"' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'rest.history.size = 50 // defaults to 50 if not specified' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'data.path = "./cerebro.db"' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'auth = {' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo '}' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'hosts = [' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo '{' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo 'host = "http://localhost:9200"' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo "name = \"$CLUSTER_NAME\"" >> /opt/$CEREBRO_VERSION/conf/application.conf
echo '}' >> /opt/$CEREBRO_VERSION/conf/application.conf
echo ']' >> /opt/$CEREBRO_VERSION/conf/application.conf








log "Creating Cerebro Service"

echo "[Unit]" >> /etc/systemd/system/cerebro.service
echo "Description=cerebro" >> /etc/systemd/system/cerebro.service
echo "After=rc.local.service elasticsearch" >> /etc/systemd/system/cerebro.service
echo "Requires=elasticsearch.service" >> /etc/systemd/system/cerebro.service

echo "[Service]" >> /etc/systemd/system/cerebro.service
echo "Type=simple" >> /etc/systemd/system/cerebro.service
echo "PIDFile=/opt/$CEREBRO_VERSION/RUNNING_PID" >> /etc/systemd/system/cerebro.service
echo "WorkingDirectory=/opt/$CEREBRO_VERSION" >> /etc/systemd/system/cerebro.service
echo "User=root" >> /etc/systemd/system/cerebro.service
echo "Group=root" >> /etc/systemd/system/cerebro.service
echo "ExecStart=/opt/$CEREBRO_VERSION/bin/cerebro" >> /etc/systemd/system/cerebro.service
echo "ExecStop=kill -9 'cat /opt/$CEREBRO_VERSION/RUNNING.PID'">>/etc/systemd/system/cerebro.service
echo "TimeoutSec=300">>/etc/systemd/system/cerebro.service
echo "[Install]">> /etc/systemd/system/cerebro.service
echo "WantedBy=multi-user.target">> /etc/systemd/system/cerebro.service

chmod 755 /etc/systemd/system/cerebro.service
chmod +x /opt/$CEREBRO_VERSION/conf/application.conf

systemctl enable cerebro
systemctl daemon-reload

#update-rc.d cerebro defaults 95 10

systemctl start cerebro
#sudo service cerebro start

#sudo systemctl daemon-reload


}


#Install Oracle Java
#------------------------
install_java

#
#Install Elasticsearch
#-----------------------
install_es

#install jmeter server agent

# Prepare configuration information
# Configure permissions on data disks for elasticsearch user:group
#--------------------------


#Format the static discovery host endpoints for Elasticsearch configuration ["",""] format
#HOSTS_CONFIG="[\"${DISCOVERY_ENDPOINTS//-/\",\"}\"]"

#Configure Elasticsearch settings
#---------------------------
#Backup the current Elasticsearch configuration file
mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.bak

# Set cluster and machine names - just use hostname for our node.name
echo "cluster.name: $CLUSTER_NAME" >> /etc/elasticsearch/elasticsearch.yml
echo "network.host: 0.0.0.0" >> /etc/elasticsearch/elasticsearch.yml


#sed -i "s/Xms2g/$JAVA_MEM/g" /etc/elasticsearch/java.options
#sed -i "s/Xmx2g/$JAVA_MEM/g" /etc/elasticsearch/java.options


if [ "$TOTAL_VM_MEM_GB" -eq "0" -o "$TOTAL_VM_MEM_GB" -eq "1" ]
then 

sed -i "s/Xms2g/$JAVA_MIN/g" /etc/elasticsearch/jvm.options
sed -i "s/Xmx2g/$JAVA_MIN/g" /etc/elasticsearch/jvm.options; fi
if [ "$TOTAL_VM_MEM_GB" -gt "1" ]
then 
sed -i "s/Xms2g/$JAVA_MIN/g" /etc/elasticsearch/jvm.options
sed -i "s/Xmx2g/$JAVA_MAX/g" /etc/elasticsearch/jvm.options; fi


#and... start the service
log "Starting Elasticsearch on ${HOSTNAME}"
update-rc.d elasticsearch defaults 95 10
sudo service elasticsearch start

log "complete elasticsearch setup and started"

install_cerebro

exit 0
